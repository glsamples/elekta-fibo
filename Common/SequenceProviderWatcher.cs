﻿using System.Diagnostics;

namespace Elekta.Fibonaccier.Common
{
    internal static class SequenceProviderWatcher
    {

        public static async Task<(long, TimeSpan)> Watch(Func<Task<long>> taskToWatch)
        {
            var stopWatch = Stopwatch.StartNew();
            var res = await taskToWatch.Invoke();
            stopWatch.Stop();
            return (res, stopWatch.Elapsed);

        }

    }


}
