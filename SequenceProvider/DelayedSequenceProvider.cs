﻿namespace Elekta.Fibonaccier.SequenceProvider
{
    internal class DelayedSequenceProvider : ISequenceProvider
    {
        private readonly ISequenceProvider sequenceProvider;
        public DelayedSequenceProvider(ISequenceProvider sequenceProvider)
        {
            this.sequenceProvider = sequenceProvider;
        }

        public async Task<long> GetElementAt(long position)
        {
            var res = await sequenceProvider.GetElementAt(position);

            var delayInMs = Random.Shared.Next(0, 1000);
            await Task.Delay(delayInMs);

            return res;

        }

    }
}

