﻿using System.Runtime.CompilerServices;

namespace Elekta.Fibonaccier.SequenceProvider
{
    public class FibonacciIterativeSequenceProvider : ISequenceProvider
    {
        private const long first_static_element = 0;
        private const long second_static_element = 1;

        public Task<long> GetElementAt(long position)
        {
            ArgGuard(position);

            var result = position;
            if (position <= second_static_element)
            {
                return Task.FromResult(result);
            }
            var lastElement = second_static_element;
            var lastButOneElement = first_static_element;
            for (var i = second_static_element + 1; i <= position; i++)
            {
                result = lastElement + lastButOneElement;
                lastButOneElement = lastElement;
                lastElement = result;
            }
            return Task.FromResult(result);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static void ArgGuard(long position)
        {
            if (position < 0)
            {
                throw new ApplicationException($"Invalid position '{position}'. {nameof(FibonacciIterativeSequenceProvider)} does not support items with number before {first_static_element}.");
            }
        }
    }
}
