﻿namespace Elekta.Fibonaccier.SequenceProvider
{

    public interface ISequenceProvider
    {
        Task<long> GetElementAt(long position);
    }
}

