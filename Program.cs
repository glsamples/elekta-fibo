﻿using Elekta.Fibonaccier.Common;
using Elekta.Fibonaccier.SequenceProvider;
using System.Text;
using System;
using System.CommandLine.Builder;
using System.CommandLine;
using System.CommandLine.Parsing;

namespace Elekta.Fibonaccier
{
    internal class Program
    {
        static async Task Main(string[] args)
        {
            var positionOption = new Option<long>(
                name: "--position",
                description: "The position of sequence item to calculate");
            positionOption.IsRequired = true;

            var rootCommand = new RootCommand("Elekta Fibonaccier");
            rootCommand.AddOption(positionOption);

            rootCommand.SetHandler((position) =>
            {
                var res = CalculateSeqItem(position);

                Console.WriteLine($"Sequence element is : {res.Item1} at position {position}.{Environment.NewLine}Measurements results: {Environment.NewLine}{res.Item2}");
                Console.ReadKey();

            }, positionOption);

            var commandLineBuilder = new CommandLineBuilder(rootCommand);

            commandLineBuilder.AddMiddleware(async (context, next) =>
            {
                try
                {
                    await next(context);
                }
                catch (ApplicationException ex)
                {
                    Console.Error.WriteLine($"Error occured : {ex.Message}");
                }
            });

            commandLineBuilder.UseDefaults();
            var parser = commandLineBuilder.Build();
            await parser.InvokeAsync(args);
        }



        private static (long, string) CalculateSeqItem(long position)
        {
            var seqProvider = CreateSequenceProvider();


            if (position < 2)
            {
                return (seqProvider.GetElementAt(position).GetAwaiter().GetResult(), "nothing to measure.");
            }

            var positionMinusOne = position - 1;
            var positionMinusTwo = position - 2;
            var itemForPositionMinusOne = SequenceProviderWatcher.Watch(() => seqProvider.GetElementAt(positionMinusOne));
            var itemForPositionMinusTwo = SequenceProviderWatcher.Watch(() => seqProvider.GetElementAt(positionMinusTwo));

            Task.WaitAll(new[] { itemForPositionMinusOne, itemForPositionMinusTwo });

            return (itemForPositionMinusOne.Result.Item1 + itemForPositionMinusTwo.Result.Item1,
                    CreateMeasurementMessage(new(itemForPositionMinusOne.Result.Item2, positionMinusOne),
                    new(itemForPositionMinusTwo.Result.Item2, positionMinusTwo))
                    );

        }


        private static string CreateMeasurementMessage((TimeSpan, long) itemForPositionMinusOne,
            (TimeSpan, long) itemForPositionMinusTwo)
        {
            var sb = new StringBuilder();

            sb.AppendLine("Calculation time for items:");
            sb.AppendLine($"at position {itemForPositionMinusOne.Item2} is {itemForPositionMinusOne.Item1.TotalMilliseconds} ms");
            sb.AppendLine($"at position {itemForPositionMinusTwo.Item2} is {itemForPositionMinusTwo.Item1.TotalMilliseconds} ms");

            var pos = itemForPositionMinusOne.Item1.TotalMilliseconds > itemForPositionMinusTwo.Item1.TotalMilliseconds ?
                itemForPositionMinusTwo.Item2 : itemForPositionMinusOne.Item2;

            sb.AppendLine($"and {pos} is faster.");

            return sb.ToString();
        }

        private static ISequenceProvider CreateSequenceProvider()
        {
            return new DelayedSequenceProvider(new FibonacciIterativeSequenceProvider());
        }
    }

}